
from celery import Celery

import os
if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'mall.settings'

app = Celery('celery_taskk')

app.config_from_object('celery_taskk.config')

app.autodiscover_tasks(['celery_taskk.sms'])