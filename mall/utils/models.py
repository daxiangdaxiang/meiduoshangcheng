from django.db import models

class BaseModel(models.Model):

    create_time = models.DateTimeField(auto_now_add=True,verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now = True,verbose_name='更新时间')
    # auto_now_add 添加对象的时间
    # auto_now  更新（添加或更改）的时间

    class Meta:
        abstract = True # 说明是抽象类，用于继承使用，数据库迁移时不会创建这个BaseModel列表
