from libs.yuntongxun.sms import CCP
from celery_task.main import app

# 任务，就是普通函数，必须用celery实例对象的task对象的task装饰器装饰
# celery实例对象会自动检测任务

@app.task(name = 'send_sms_code')
def send_sms_code(mobile, sms_code):

    ccp = CCP()
    ccp.send_template_sms(mobile, [sms_code, 5], 1)
