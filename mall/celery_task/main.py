from celery import Celery

import os
if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'mall.settings'
# 为celery使用django配置文件进行设置。需要加载当前工程的配置信息
app = Celery('celery_task')
# 创建celery实例对象  参数main 一般就是设置文件夹的名字（路径）
app.config_from_object('celery_task.config')
# 加载配置信息（broker文件）config里面是 redis信息 ---此处用redis作为broker
app.autodiscover_tasks(['celery_tasks.sms'])
# 自动监测tasks任务