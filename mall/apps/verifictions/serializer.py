from rest_framework import serializers
from django_redis import get_redis_connection

class RegistersmscodeSerializer(serializers.Serializer):

    text = serializers.CharField(max_length=4,min_length=4,label='图片验证码',required=True)
    image_code_id = serializers.UUIDField(label='uuid',required=True)

    def validate(self, attrs):

        text = attrs.get('text')
        image_code_id = attrs['image_code_id']
        # 提取数据 text image_code_id
        redis_conn = get_redis_connection('code')
        redis_text = redis_conn.get('img_%s'%image_code_id).decode()
        # 连接redis数据库 获取数据image_code
        # 1 redis 获取的数据是bytes类型  需要转化成字符串类型 decode（）
        # 2 注意大小写！
        if not redis_text:
            return serializers.ValidationError('图片验证码已过期')
        if redis_text.upper() != text.upper():
            raise serializers.ValidationError('图片验证码不正确')
        return attrs



