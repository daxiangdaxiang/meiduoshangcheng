from django.http import HttpResponse
from django.shortcuts import render
import random
from rest_framework.response import Response
from rest_framework.views import APIView
from libs.captcha.captcha import captcha
from django_redis import get_redis_connection
from libs.yuntongxun.sms import CCP
from verifictions.serializer import RegistersmscodeSerializer


class Registerimage_code_idAPIView(APIView):
    def get(self,request,image_code_id):
        text,image = captcha.generate_captcha()
        # 生成图片验证码，返回txt和图片格式，用2个变量拆包
        redis_conn = get_redis_connection('code')
        # redis提供了get_redis_connection方法，通过调用get_redis_connection方法传递redis的配置名称可获取到redis的连接对象，通过redis连接对象可以执行redis命令。
        redis_conn.setex('img_%s'%image_code_id, 600, text)
        # 保存验证码到redis     name              time  value  三个值
        return HttpResponse(image,content_type='image/jpeg')

class RegistersmscodeAPIView(APIView):
    def get(self,request,mobile):
        params = request.query_params.dict()
        # 接收参数
        serializer = RegistersmscodeSerializer(data=params)
        # 参数传递给序列化器
        serializer.is_valid(raise_exception=True)
        # 校验
        sms_code = '%06d'% random.randint(0,999999)
        print(sms_code)
        # 生成短信验证码
        redis_conn = get_redis_connection('code')
        # 链接redis数据库
        redis_conn.setex('sms_' + mobile, 5*60, sms_code)
        # 保存到数据库
        # CCP().send_template_sms(mobile,[sms_code, 5], 1)
        #    发送短信           手机号 【验证码，过期时间】短信模板
        from celery_tasks.sms.tasks import send_sms_code
        # 任务(函数) 需要使用 任务(函数名)名.delay() 调用    delay 方法的参数和函数名（任务）一致
        send_sms_code.delay(mobile,sms_code)

        return Response({'msg':'ok'})

