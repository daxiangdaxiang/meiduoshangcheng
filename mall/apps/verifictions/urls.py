

from . import views
from rest_framework.urls import url

urlpatterns = [
    url(r'imagecodes/(?P<image_code_id>.+)/', views.Registerimage_code_idAPIView.as_view()),
    url(r'smscodes/(?P<mobile>1[345789]\d{9})/',views.RegistersmscodeAPIView.as_view()),
]

