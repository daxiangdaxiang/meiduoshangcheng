import base64
import pickle
from django.shortcuts import render

# Create your views here.
from django_redis import get_redis_connection
# from requests import Response
from rest_framework import status
from rest_framework.response import Response
from rest_framework import response
from rest_framework.views import APIView
from carts.serializers import CartSerialzier, CartlistSerialzier, CartDeleteSerializer
from goods.models import SKU

"""
1、未登录用户：购物车信息保存在cookie中
   登录用户：购物车信息保存在数据库mysql中。现在保存在redis

2、 需要保存的数据： 商品id 商品个数 选中状态
3、 组织cookie的数据格式
    cart:{
        sku_id:{'count':4,'selected':True},
        sku_id:{'count':8, 'selected':False},
    }

    组织redis数据结构-----redis数据是保存在内存中的
    原则：尽量占用少的内存来实现功能
    sku_id ,count, selected

    hash   hash_key:  property:value
            cart_userid:sku_id:count

            cart_user1:  1:5
                         2:3
                         3:5
            只记录选中商品的id
            cart_user1:  3:10

    set     set_key:  [value1,value2]
            cart_selected_user1:{2}

4 如何区分登录还是未登录用户
         user.is_authenticated
         user.is_authenticated   False 表示用户未认证登录
         user.is_authenticated   True  表示用户认证登录

"""

class CartAPIView(APIView):
    """
    POST    cart/  添加购物车数据
    GET     cart/  获取购物车数据
    PUT     cart/  修改购物车数据
    DELETE  cart/  删除购物车数据
    """
    """
    用户提交了token 如果这个token如果过期了/被篡改了，在前端发送post请求时，会自动经过系统认证，返回401未认证错误，就不能实现添加购物车的功能
    这时，我们应该先让用户添加购物车（未登录状态，添加到cookie）
    所以不应该先认证。---通过 def perform_authentication(self, request): 可以取消系统自动认证


    """
    def perform_authentication(self, request):
        """重写父类的验证方法，此处直接pass，表示不验证，直接通过，可以直接进入下面的请求流程"""
        pass

    """
    用户点击添加购物车时，前端需要传递数据：商品id 商品数量 用户信息（如果登录）
    后端：
    1、 接收数据 sku_id、 count 选中状态可以不传，默认选中
    2、 校验数据 （判断商品是否存在，判断库存）
    3、 获取验证的数据
    4、 获取用户信息
    5、 判断用户登录状态
    6、 登录用户 保存到redis
        6.1 链接redis
        6.2 hash set
        6.3 返回响应
    7、未登录用户 保存到cookie
        7.1 获取用户购物车中的数据
        7.2 判断用户是否存在
        7.3 如果存在相同的商品，我们需要对商品数量进行累加
        7.4 如果不存在相同的商品，我们添加数据
        7.5 返回响应
    """
    def post(self,request):
        # 1、 接收数据
        data = request.data
        # 2、 校验数据 （判断商品是否存在，判断库存）
        serializer = CartSerialzier(data=data)
        serializer.is_valid(raise_exception=True)
        # 3、 获取验证的数据
        sku_id = serializer.validated_data.get('sku_id')
        count = serializer.validated_data.get('count')
        selected = serializer.validated_data.get('selected')
        # 4、 获取用户信息
        try:
            user = request.user
        except Exception as e:
            user = None

        # 5、 判断用户登录状态  request.user.is_authenticated
        if user is not None and user.is_authenticated:  # 未登录用户会返回一个匿名用户，所以要加一个user.is_authenticated 来确认
            # 6、 登录用户 保存到redis
            #     6.1 链接redis
            redis_conn = get_redis_connection('cart')
            #     6.2 hash set   hset(key, field, value)：向名称为key的hash中添加元素field<—>value
            redis_conn.hset('cart_%s'%user.id, sku_id, count)
            #     set    sadd(key, member)：向名称为key的set中添加元素member
            if selected:
                redis_conn.sadd('cart_selected_%s'%user.id,sku_id)
            # 6.3 返回响应
            return Response(serializer.data)

        # 7、未登录用户 保存到cookie
        else:
            # 7.1 获取用户购物车中的数据
            cookie_str = request.COOKIES.get('cart')
            #     7.2 判断用户是否存在
            if cookie_str is not None:  # 说明有数据
                decode = base64.b64decode(cookie_str)   # 将base64的数据解码
                cookie_cart = pickle.loads(decode)      # 将二进制转化为字典
            else:   # 说明没有数据
                cookie_cart = {}   # 初始化一个字典
            #     7.3 如果存在相同的商品，我们需要对商品数量进行累加
            # # cookie_cart = {1: {count:10,selected:True}}
            if sku_id in cookie_cart:    # Python 字典 in 操作符用于判断键是否存在于字典中
                original_count = cookie_cart[sku_id]['count']   # 原个数获取
                count += original_count
            #     7.4 如果不存在相同的商品，我们添加数据
            cookie_cart[sku_id] = {
                'count':count,
                'selected':selected
            }
            # 7.5 对字典进行base64 的处理
            # 7.5.1  pickle.dumps 将字典转化成二进制
            dumps = pickle.dumps(cookie_cart)
            # 7.5.2   base64.b64encode 将二进制进行base64 编码   base64 的参数必须是bytes类型
            encode = base64.b64encode(dumps)
            # 7.5.3  将bytes类型转化为str
            value = encode.decode()
            # 7.6 返回响应
            response = Response(serializer.data)
            # response = Response(serializer.data)
            response.set_cookie('cart',value)
            return response

    """
       当用户获取购物车数据的时候,我们需要让前端传递一个用户信息过来(如果登陆则传递)

       1.接收用户信息
       2.根据用户信息进行判断
       3.登陆用户从redis中获取数据
           3.1 连接redis
           3.2 获取hash          cart_userid:            sku_id:count
               获取set数据       cart_selected_userid:     sku_id
           3.3 需要根据skuid 获取商品的详细信息 [SKU,SKU,SKU]
           3.4 对列表数据进行序列化器处理
           3.5 返回相应
       4.未登录用户从cookie中获取数据
           4.1 获取cookie中的 cart数据
           4.2 判断cart数据是否存在
               如果存在需要进行base64解码        {sku_id:{count:4,selected:True}}
               如果不存在 初始化 cookie_cart
           4.3 需要根据skuid 获取商品的详细信息 [SKU,SKU,SKU]
           4.4 对列表数据进行序列化器处理
           4.5 返回相应


       1.接收用户信息
       2.根据用户信息进行判断
       3.登陆用户从redis中获取数据
           3.1 连接redis
           3.2 获取hash          cart_userid:            sku_id:count
               获取set数据       cart_selected_userid:     sku_id
       4.未登录用户从cookie中获取数据
           4.1 获取cookie中的 cart数据
           4.2 判断cart数据是否存在
               如果存在需要进行base64解码        {sku_id:{count:4,selected:True}}
               如果不存在 初始化 cookie_cart

       5 需要根据skuid 获取商品的详细信息 [SKU,SKU,SKU]
       6 对列表数据进行序列化器处理
       7 返回相应
       """
    def get(self,request):
        """获取购物车信息"""
        # 1 接收用户信息
        try:
            user = request.user
        except Exception as e:
            user = None
        # 2 根据用户信息进行判断
        if user is not None and user.is_authenticated:
            redis_conn = get_redis_connection('cart')
            # 获取hash  carts_userid ：{sku_id:count，sku_id:count}
            # hgetall(key)：返回名称为key的hash中所有的键（field）及其对应的value
            redis_sku_id_count = redis_conn.hgetall('cart_%s'%user.id)
            # 获取set数据  cart_selected_userid : sku_id
            redis_select_ids = redis_conn.smembers('cart_selected_%s'%user.id)
            # cart: {
            #     sku_id: {'count': 4, 'selected': True},
            #     sku_id: {'count': 8, 'selected': False},   }
            # 将redis数据转化为和cookie数据一样的格式
            cookie_cart = {}
            for sku_id, count in redis_sku_id_count.items():

                if sku_id in redis_select_ids:
                    selected = True
                else:
                    selected = False

                cookie_cart[sku_id] = {
                    'count': count,
                    'selected':selected
                }

        else:
            # 未登录用户从cookie获取数据
            cookie_str = request.COOKIES.get('cart')
            if cookie_str is not None:
                decode = base64.b64decode(cookie_str)
                cookie_cart = pickle.loads(decode)
            else:
                cookie_cart = {}
        # 需要返回 id  price count default_image_url name selected
        ids = []
        for sku_id in cookie_cart:
            ids.append(sku_id)
        ids = cookie_cart.keys()      # 获取字典的所有key
        skus = SKU.objects.filter(id__in=ids)

        # 实例化对象没有count 和selected属性，需要动态添加这两个属性
        for sku in skus:
            sku.count = cookie_cart[sku_id]['count']
            sku.selected = cookie_cart[sku_id]['selected']
        serializer = CartlistSerialzier(instance=skus, many=True)

        return Response(serializer.data)

    """    修改购物车的数据

    1.后端接收数据 sku_id,count,selected
    2.验证数据
    3. 获取验证之后的数据
    4. 获取用户信息
    5. 根据用户信息进行判断
    6. 登陆用户redis
        6.1 连接redis
        6.2 更新数据
            hash
            set
        6.3 返回相应
    7. 未登录用户cookie
        7.1 获取cookie数据
        7.2 判断cart数据是否存在
        7.3 更新数据
        7.4 返回相应
    """
    def put(self,request):
        data = request.data
        serialzier = CartSerialzier(data=data)
        serialzier.is_valid(raise_exception=True)

        sku_id = serialzier.validated_data.get('sku_id')
        count = serialzier.validated_data.get('count')
        selected = serialzier.validated_data.get('selected')

        try:
            user = request.user
        except Exception as e:
            user = None

        if user is not None and user.is_authenticated:

            redis_conn = get_redis_connection('cart')
            redis_conn.hset('cart_%s'%user.id,sku_id,count)
            # hset(key, field, value)：向名称为key的hash中添加元素field<—>value
            if selected:
                redis_conn.sadd('cart_selected_%s'%user.id,sku_id)
                # sadd(key, member)：向名称为key的set中添加元素member
                # 将member元素加入到集合key当中，已经存在于集合的 member元素将被忽略。假如 key不存在，则创建一个只包含member元素作成员的集合。
            else:
                redis_conn.srem('cart_selected_%s'%user.id,sku_id)
                # 移除集合 key 中的一个或多个 member 元素，不存在的 member 元素会被忽略。
            return Response(serialzier.data)
        else:
            cookie_str = request.COOKIES.get('cart')
            if cookie_str is not None:

                decode = base64.b64decode(cookie_str.encode())
                cookie_cart = pickle.loads(decode)
            else:
                cookie_cart = {}

            if sku_id in cookie_cart:
                cookie_cart[sku_id]= {
                    'count': count,
                    'selected': selected
                }

            response = Response(serialzier.data)
            encode = base64.b64encode(pickle.dumps(cookie_cart))
            value = encode.decode()
            response.set_cookie('cart',value)
            return response

    """
       当用户点击删除按钮的时候,前端需要将 用户信息(登陆用户)和商品id
       提交给后端

       1. 接收数据
       2. 校验数据
       3. 获取校验的数据
       4. 获取用户信息
       5. 根据用户信息进行判断
       6. 登陆用户redis
           6.1 连接redis
           6.2 删除数据
               hash
               set
           6.3 返回相应
       7. 未登录用户cookie
           7.1 获取cookie数据
           7.2 判断cart数据是否存在
           7.3 删除数据
           7.4 返回相应

       """

    def delete(self,request):
        data = request.data
        serializer = CartDeleteSerializer(data=data)
        serializer.is_valid(raise_exception=True)

        sku_id = serializer.validated_data.get('sku_id')

        try:
            user = request.user
        except Exception as e:
            user = None

        if user is not None and user.is_authenticated:

            redis_coon = get_redis_connection('cart')
            redis_coon.hdel('cart_%s'%user.id,sku_id)
            redis_coon.srem('cart_selected_%s'%user.id,sku_id)

            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            cookie_str = request.COOKIES.get('cart')
            if cookie_str is not None:
                cookie_cart = pickle.loads(base64.b64decode(cookie_str.encode()))
            else:
                cookie_cart = {}

            if sku_id in cookie_cart:
                del cookie_cart[sku_id]

            response = Response(status=status.HTTP_204_NO_CONTENT)
            value = base64.b64encode(pickle.dumps(cookie_cart)).decode()

            response.set_cookie('cart',value)
            return response


