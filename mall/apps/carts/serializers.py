from rest_framework import serializers
from goods.models import SKU
class CartSerialzier(serializers.Serializer):

    sku_id = serializers.IntegerField(required=True)
    count = serializers.IntegerField(required=True)
    selected = serializers.BooleanField(required=False,default=True)

    def validate(self, attrs):
        sku_id = attrs.get('sku_id')
        # 1 判断商品是否存在
        try:
            sku = SKU.objects.get(pk=sku_id)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')
        # 2 判断库存
        count = attrs['count']
        if sku.stock < count:
            raise serializers.ValidationError('库存不足')
        return attrs


class CartlistSerialzier(serializers.ModelSerializer):

    count = serializers.IntegerField(label='个数')
    selected = serializers.BooleanField(label='选中状态')
    class Meta:
        model = SKU
        fields = ['id', 'price', 'default_image_url', 'name', 'count', 'selected']

class CartDeleteSerializer(serializers.Serializer):

    sku_id = serializers.IntegerField(required=True)

    def validate(self, attrs):
        sku_id = attrs.get('sku_id')

        try:
            SKU.objects.get(pk=sku_id)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')

        return attrs