import pickle
import base64
from django_redis import get_redis_connection

"""
redis 数据结构  一个账户只有一个user
        1、hash
        {cart_userid:
                    {sku_id:count, sku_id:count}
        }
        2、set
        {cart_selected_userid:
                            {sku_id,sku_id}
        }

cookie 数据结构
        {
            sku_id:{count:100,selected:True},
            sku_id:{count:100,selected:False},
        }

"""


def merge_cookie_to_redis(request, user, response):
    """合并购物车功能"""
    # 1 获取cookie数据
    cookie_str = request.COOKIES.get('cart')
    if cookie_str is not None:
        cookie_cart = pickle.loads(base64.b64decode(cookie_str.encode()))
        # 2 获取redis数据 # {b'1':b'5',}
        redis_conn = get_redis_connection('cart')
        red_id_count = redis_conn.hgetall('cart_%s'%user.id)
        # 3 做数据的初始化  (将redis的bytes类型进行转换)
        merge_cart = {}
        for sku_id, count in red_id_count.items():
            # 将{b'1':b'5',} bytes类型转换为int类型
            merge_cart[int(sku_id)] = int(count)
        selected_ids = []

        # 4 对cookie数据进行遍历
        for sku_id, count_selected_dict in cookie_cart.items():
            # if sku_id not in merge_cart:  # 如果sku_id 没有和redis重复，添加
            #     merge_cart[sku_id]=count_selected_dict['count']
            # else:   # id重复, 我们以cookie数据为准，更改  两种情况，是一样的处理语句
            #     merge_cart[sku_id]=count_selected_dict['count']
            merge_cart[sku_id] = count_selected_dict['count']  # 这里sku_id count 是什么类型？
            if count_selected_dict['selected']:
                selected_ids.append(sku_id)
        # 5 将合并的数据保存到redis中  merge_cart  {sku_id:count,sku_id:count}
        #hmset(key, field1, value1,…, field  N, value  N)：同时将多个 field-value (域-值)对设置到哈希表 key 中。不存在，则创建，否则，修改
        redis_conn.hmset('cart_%s'%user.id, merge_cart)
        if len(selected_ids)>0:
            redis_conn.sadd('cart_selected_%s'%user.id,*selected_ids)
        # 6 删除cookie数据
        response.delete_cookie('cart')
        return response
    return response


