from itsdangerous import BadSignature
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from mall import settings
def generic_verify_url(user_id):
    s = Serializer(secret_key=settings.SECRET_KEY,expires_in=3600)
    # 创建序列化器
    data = {
        'id': user_id
    }
    # 组织数据
    token = s.dumps(data)
    # 对数据加密
    return 'http://www.meiduo.site:8080/success_verify_email.html?token=' + token.decode()

def check_token(token):
    s = Serializer(secret_key=settings.SECRET_KEY,expires_in=3600)
    # 创建序列化器
    try:
        data= s.loads(token)
    except BadSignature:
        return None
    # 对数据解密
    return data.get('id')
    # 获取数据

