from django.http import HttpResponse
from django.shortcuts import render
from django_redis import get_redis_connection
from rest_framework.response import Response

from goods.models import SKU
from users.models import Address

from users.utils import check_token
from .serializer import RegisterUserSerializer, UserCenterInfoSerializer, UserEmailSerializer, AddressSerializer, \
    AddUserBrowsingHistorySerializer, SKUSerializer, default_addressSeializer
from users.models import User
from rest_framework.views import APIView


# 用户名验证
class RegisterUsernameCountAPIView(APIView):
    def get(self,request,username):
        count = User.objects.filter(username=username).count()
        return Response({'count':count,'username':username})

# 手机号验证
class RegistermobileCountAPIView(APIView):
    def get(self,request,mobile):
        count = User.objects.filter(mobile=mobile).count()
        return Response({'count':count, 'mobile':mobile})

# 注册功能
class RegisterUserAPIView(APIView):
    def post(self,request):
        data = request.data
        # code = data['sms_code']
        # mobile = data['mobile']
        # redis_conn = get_redis_connection('code')
        # redis_code = redis_conn.get('sms_%s' % mobile).decode()
        serializer = RegisterUserSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

"""
当用户注册成功后，自动实现登录
因为我们采用的jwt
也就是说，用户注册成功后，应该返回一个token给响应同时浏览器把接收到的token保存

用户注册成功后，应该返回一个token

序列化的时候，多一个token

序列化的时候，多一个token
序列化器原理：序列化器根据序列化器的字段 token 来获取模型token中数据
"""


"""
1 明确需求
2 梳理思路
3 确定请求方式和路由
4 确定视图
5 按照步骤开发

个人中心的接口必须是登录用户才可以访问的

前端需要传递用户的信息
1 接收用户的信息
2 查询用户的信息 uesr
3 将user 转化为字典数据

GET /users/infos/

"""
from rest_framework.permissions import IsAuthenticated
# 方法1
# class UserCenterInfoAPIView(APIView):
#     permission_classes = [IsAuthenticated]
#     # 在当前视图中设置权限：登录用户才可以
#     def get(self,request):
#         user = request.user
#         # 接收用户信息
#         # 查询用户信息，user 因为直接接收到的uer 所以不用查询了
#         serializer = UserCenterInfoSerializer(user)
#         # 将user转化为json数据   ??? 前端传过来的是不是对象？
#         return Response(serializer.data)

# 方法2
from rest_framework.generics import RetrieveAPIView, CreateAPIView, ListAPIView,UpdateAPIView,DestroyAPIView, \
    GenericAPIView


class UserCenterInfoAPIView(RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = UserCenterInfoSerializer

    # 通过我们分析三级视图的原理：当前的 get_object 不嫩满足我们的需求
    def get_object(self):
        return self.request.user

"""
需要添加一个邮件激活的状态
1 当用户点击保存的时候，首先把用户的邮箱内容更新到数据库中
2  2.1 同时要给这个邮箱发送一封激活邮件  2.2 激活邮件的内容
3 用户点击邮件的时候，激活
"""

"""
用户点击保存的时候，首先把用户邮箱更新到数据库中

前端 将用户输入的邮箱内容通过axios请求发送给后端

这个接口必须是登录用户才可以访问
后端
1 接收数据
2 验证数据
3 更新数据
4 返回数据

PUT /user/emails/
"""
# 邮箱设置 发送邮件 方法1
class UserEmailAPIView(APIView):
    permission_classes = [IsAuthenticated]
    def put(self,request):
        data = request.data
        # 1 接收数据
        serializer = UserEmailSerializer(instance=request.user,
                                         data=data)
        serializer.is_valid(raise_exception=True)
        # 2 验证数据
        serializer.save()
        # 更新数据 发送激活邮件
        return Response(serializer.data)

# 邮箱设置 发送邮件 方法2
# from rest_framework.generics import UpdateAPIView
# class UserEmailAPIView(UpdateAPIView):
#     permission_classes = [IsAuthenticated]
#     def get_object(self):
#         return self.request.user
#     serializer_class = UserEmailSerializer


from rest_framework import status
class UserEmailActiveAPIView(APIView):
    def get(self,request):
        token = request.query_params.get('token')
        if token is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        # 接收token
        user_id = check_token(token)
        if user_id is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        # 验证token 获取user_id
        user = User.objects.get(pk=user_id)
        # 查询用户信息
        user.email_active = True
        user.save()
        # 改变用户激活状态
        return Response({'msg': 'ok'})
"""
新增地址
POST  /users/addresses/
地址列表
GET
删除地址
DEL
更改地址
PUT
"""
from users.models import User
class AddressAPIView(CreateAPIView,ListAPIView):
    """新增收货地址 获取地址列表"""
    pagination_class = None
    serializer_class = AddressSerializer
    queryset = Address.objects.all()

    def list(self, request, *args, **kwargs):
        data = Address.objects.all()
        serializer = AddressSerializer(data, many=True)
        data = {
            'addresses':serializer.data,
            'default_address_id': request.user.default_address.id
        }
        return Response(data)


class AddressDetailAPIView(UpdateAPIView,DestroyAPIView):
    pagination_class = None
    queryset = Address.objects.all()
    serializer_class = AddressSerializer


# /users/addresses/address.id/title/
class ChangeAddressTitle(APIView):
    """修改地址标题"""
    def put(self,request,address_id):
        data = request.data
        address = Address.objects.get(id=address_id)
        address.title = data.get('title')
        address.save()
        return Response({'message': 'OK'}, status=status.HTTP_200_OK)

class default_addressAPIView(APIView):
    """设置默认地址"""
    def put(self, request, address_id):

        address = Address.objects.get(id=address_id)
        request.user.default_address = address
        request.user.save()
        return Response({'message': 'OK'}, status=status.HTTP_200_OK)


# class AddressAPIView(APIView):
#     permission_classes = [IsAuthenticated]
#     def post(self, request):
#         data = request.data
#         # 接收数据
#         serializer = AddressSerializer(data=data,context={'request':request})
#         serializer.is_valid(raise_exception=True)
#         # 验证数据
#         serializer.save()
#         # 保存数据
#         return Response(serializer.data)
           # 返回数据
    # def get(self,request):
    #     data = Address.objects.all()
    #     serializer = AddressSerializer(data=data, many=True)
    #     serializer.is_valid()
    #     return Response(serializer.data)


    # def delete(self,request,pk):
    #     data =
    #
    # def put(self,request,pk):
    #     pass




"""


1 必须是登录用户 前端传的token
2 接收前段提供的sku_id
3 对数据进行校验
4 数据入库（redis）
5 返回响应

POST /user/histroies
"""
from rest_framework.generics import CreateAPIView

class UserHistoryAPIView(CreateAPIView):
    """添加浏览历史记录"""
    permission_classes = [IsAuthenticated]

    serializer_class = AddUserBrowsingHistorySerializer

    def get(self,request):
        """获取浏览历史记录"""
        user = request.user
        # 1 获取redis的数据
        redis_conn = get_redis_connection('history')
        ids = redis_conn.lrange('history_%s'%user.id,0,4)
        # lrange(key, start, end)：返回名称为key的list中start至end之间的元素（下标从0开始，下同）

        # 2 根据id获取商品的数据
        # skus = SKU.objects.filter(pk__in=ids)    # 此方法不会按照浏览顺序排序，不满足需求
        skus = []
        for id in ids:
            sku = SKU.objects.get(pk=id)
            skus.append(sku)
        # 3 返回字典（json数据）
        serializer = SKUSerializer(skus,many=True)

        return Response(serializer.data)

from rest_framework_jwt.views import ObtainJSONWebToken
class MergeLoginView(ObtainJSONWebToken):

    def post(self,request,*args,**kwargs):
        # 1 先调用父类
        response = super().post(request,*args,**kwargs)

        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            from carts.utils import merge_cookie_to_redis
            response = merge_cookie_to_redis(request,user,response)
        return response

