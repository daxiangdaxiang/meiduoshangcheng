from django.conf.urls import url
from rest_framework import views
from . import views
from rest_framework_jwt.views import obtain_jwt_token
urlpatterns = [
    url(r'^usernames/(?P<username>\w{5,20})/count/$', views.RegisterUsernameCountAPIView.as_view()),
    url(r'^phones/(?P<mobile>1[345789]\d{9})/count/$', views.RegistermobileCountAPIView.as_view()),
    url(r'^$', views.RegisterUserAPIView.as_view()),
    # 添加jwt认证
    # url(r'^auths/', obtain_jwt_token),
    # jwt 是先使用django 自带的认证方式进行认证，认证成功才生成token
    url(r'^auths/', views.MergeLoginView.as_view()),
    url(r'^infos/$', views.UserCenterInfoAPIView.as_view()),
    url(r'^emails/$',views.UserEmailAPIView.as_view()),
    url(r'^emails/verification/$',views.UserEmailActiveAPIView.as_view()),
    url(r'^addresses/$',views.AddressAPIView.as_view()),
    url(r'^addresses/(?P<pk>\d+)/$',views.AddressDetailAPIView.as_view()),
    url(r'^browerhistories/$', views.UserHistoryAPIView.as_view()),
    url(r'^addresses/(?P<address_id>\d+)/status/$', views.default_addressAPIView.as_view()),
    url(r'^addresses/(?P<address_id>\d+)/title/$', views.ChangeAddressTitle.as_view()),

]