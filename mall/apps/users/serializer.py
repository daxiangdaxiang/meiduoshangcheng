from django_redis import get_redis_connection
from rest_framework import serializers

from mall import settings
from users.utils import generic_verify_url
from .models import User, Address
import re

# 抽取 获取token的方法
def get_token(instances):
    from rest_framework_jwt.settings import api_settings

    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    payload = jwt_payload_handler(instances)
    token = jwt_encode_handler(payload)

    return token


# 当用户点击注册的时候 需要将 用户名,密码,确认密码,手机号,短信验证码,是否同意协议
# 发送给后端
class RegisterUserSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(label='校验密码', allow_null=False, allow_blank=False,write_only=True)
    # write_only 只写，只在反序列化的时候使用，序列化的时候忽略此字段。
    sms_code = serializers.CharField(label='短信验证码', max_length=6, min_length=6, allow_null=False, allow_blank=False,write_only=True)
    allow = serializers.CharField(label='是否同意协议', allow_null=False, allow_blank=False, write_only=True)
    token = serializers.CharField(read_only=True,label='token')
    class Meta:
        model = User
        fields = ('id','token','username','password','password2', 'mobile', 'sms_code', 'allow')
        extra_kwargs = {
            'id':{'read_only':True},
            'username':{
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,  # 返回数据时不需要返回密码，这样设置 序列化时（转json时）忽略此字段
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }

        }

    def validate_mobile(self,value):
        if not re.match(r'1[345789]\d{9}', value):
            raise serializers.ValidationError('手机号码格式不正确')
        return value

    def validate_allow(self,value):
        if value != 'true':
            raise serializers.ValidationError('请同意协议')
        return value


    def validate(self, attrs):
        password = attrs['password']
        password2 = attrs['password2']

        if password != password2:
            raise serializers.ValidationError('密码不一致')

        code = attrs['sms_code']
        mobile = attrs['mobile']
        redis_conn = get_redis_connection('code')
        redis_code = redis_conn.get('sms_%s' % mobile).decode()
        # 从redis取出的值是bytes格式，要转化成字符串 decode()

        if code != redis_code:
            raise serializers.ValidationError('短信验证码不正确')

        return attrs

    def create(self, validated_data):

        del validated_data['password2']
        del validated_data['sms_code']
        del validated_data['allow']
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        """
        思路：
        需求：注册成功时自动保存登录状态--
             需要传回token
             即序列化时生成token
             而序列化原理是：序列化器根据 序列化器的token字段来获取模型中token数据
             所以，模型中要有token
             所以，需要在user中添加token属性 可以通过实例对象动态添加token属性

             而token 是不需要写入模型的，所以设置read_only
        """
        # from rest_framework_jwt.settings import api_settings
        #
        # jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        # jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        #
        # payload = jwt_payload_handler(user)
        # token = jwt_encode_handler(payload)
        # user.token = token
        token = get_token(user)
        user.token = token
        return user

class UserCenterInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id','username','email','mobile')


class UserEmailSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id','email')
        extra_kwargs = {
            'email': {
                'required': True
            }
        }
    def update(self, instance, validated_data):
        email = validated_data.get('email')
        instance.email = email
        instance.save()
        # 保存邮箱
        from django.core.mail import send_mail
        # subject, message, from_email, recipient_list
        subject = '美多商城激活邮件'
        # 邮件主题
        message = '请激活'
        # 邮件内容
        from_email = settings.EMAIL_FROM
        # 发件人
        recipient_list = [email]
        # 收件人列表
        verify_url = generic_verify_url(instance.id)
        # 封装的思想， 获取token 拼接激活链接
        html_message = '<p>尊敬的用户您好！</p>' \
                   '<p>感谢您使用美多商城。</p>' \
                   '<p>您的邮箱为：%s 。请点击此链接激活您的邮箱：</p>' \
                   '<p><a href="%s">%s<a></p>' % (email, verify_url, verify_url)

        from celery_tasks.email.tasks import send_celery_email
        send_celery_email.delay(
            subject=subject,
            message=message,
            from_email=from_email,
            recipient_list=recipient_list,
            html_message=html_message
        )
        return instance

class AddressSerializer(serializers.ModelSerializer):
    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)
    province_id = serializers.IntegerField(label='省ID', required=True)
    city_id = serializers.IntegerField(label='市ID', required=True)
    district_id = serializers.IntegerField(label='区ID', required=True)
    mobile = serializers.RegexField(label='手机号', regex=r'^1[3-9]\d{9}$')

    # user = serializers.CharField(required=True)

    class Meta:
        model = Address
        exclude = ('user','is_deleted', 'create_time', 'update_time')

    def create(self, validated_data):
        # Address模型类中有user属性,将user对象添加到模型类的创建参数中
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)


# class AddressSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = Address
#         # 序列化器的字段怎么选择？是不是只选择前段传过来的字段？对于非必需字段，怎么处理？
#         fields = ['user', 'receiver', 'province','city','district', 'place','mobile','tel','email']

from goods.models import SKU
class AddUserBrowsingHistorySerializer(serializers.Serializer):
    sku_id = serializers.IntegerField(label='商品编号',min_value=1,required=True)

    def validate_sku_id(self,value):

        # 检查商品是否存在
        try:
            SKU.objects.get(pk=value)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')
        return value

    def create(self, validated_data):
        """原来的create方法不能满足需求，重写"""
        sku_id = validated_data['sku_id']

        user = self.context['request'].user
        # 1 连接redis
        redis_conn = get_redis_connection('history')
        # 2 删除之前的重复数据
        # lrem(key, count, value)：删除count个名称为key的list中值为value的元素。count为0，删除所有值为value的元素，count > 0
        # 从头至尾删除count个值为value的元素，count < 0 ，从尾到头删除 | count | 个值为value的元素。
        redis_conn.lrem('history_%s'%user.id,0,sku_id)
        # 3 添加数据   lpush(key, value)：在名称为key的list头添加一个值为value的 元素
        redis_conn.lpush('history_%s'%user.id,sku_id)
        # 4 对数据进行裁剪  ltrim(key, start, end)：截取名称为key的list，保留start至end之间的元素
        redis_conn.ltrim('history_%s'%user.id,0,4)

        return validated_data

class SKUSerializer(serializers.ModelSerializer):

    class Meta:
        model = SKU
        fields = ('id','name', 'price','default_image_url','comments')
from .models import User
class default_addressSeializer(serializers.ModelSerializer):

    default_address = serializers.IntegerField(required=True)

    class Meta:
        model = User
        fields = ['default_address']

    def validate_address_id(self, value):

        # 检查地址是否存在
        try:
            Address.objects.get(pk=value)
        except Address.DoesNotExist:
            raise serializers.ValidationError('地址不存在')
        return value

    def update(self, instance, validated_data):

        instance.default_address = validated_data.get('default_address')
        instance.save()
        return instance
