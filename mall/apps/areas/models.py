# from django.db import models
# class Areas(models.Model):
#     name = models.CharField(max_length=20, verbose_name='名称')
#     parent = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, related_name='subs',blank=True,verbose_name='上级行政区划')
#     class Meta:
#         db_table = 'tb_area',
#         verbose_name = '行政区划'
#         verbose_name_plural = '行政区划'
#     def __str__(self):
#         return self.name

from django.db import models
class Area(models.Model):
    """
    行政区划
    """
    name = models.CharField(max_length=20, verbose_name='名称')
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, related_name='subs', null=True, blank=True, verbose_name='上级行政区划')

    class Meta:
        db_table = 'tb_areas'
        verbose_name = '行政区划'
        verbose_name_plural = '行政区划'

    def __str__(self):
        return self.name