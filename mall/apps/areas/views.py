from django.shortcuts import render
from rest_framework.views import APIView
from areas.models import Area
from areas.serializers import AreaSerializer, SubsAreaSerialzier
from rest_framework_extensions.cache.mixins import CacheResponseMixin
from rest_framework.viewsets import ReadOnlyModelViewSet
# Create your views here.


class AreaModelViewSet(CacheResponseMixin, ReadOnlyModelViewSet):
    # serializer_class = AreaSerializer
    # queryset = Area.objects.all()
    pagination_class = None    # 因为在settings里设置了分页功能，所有视图函数都会分页，所以，在这里设置一下取消分页

    def get_queryset(self):
        if self.action == 'list':
            # action 视图集的一个属性，用来区分视图集中包含的 方法Retrieve 和 List
            return Area.objects.filter(parent=None)
        else:
            return Area.objects.all()

    def get_serializer_class(self):
        if self.action=='list':
            return AreaSerializer
        else:
            return SubsAreaSerialzier
