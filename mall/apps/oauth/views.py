from QQLoginTool.QQtool import OAuthQQ
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from mall import settings
from oauth.models import OAuthQQUser
from oauth.serializers import OauthQQUserSerializer
from oauth.utils import generic_open_id

"""
1、明确需求
2、梳理思路
3、确定请求方式和路由
4、确定视图
5、按照步骤进行开发
当客户点击qq图标的时候，返回一个url（根据qq的接口文档，拼接一个url）给前端
GET  /oauth/qq/status/

"""
class OAuthQQURLAPIView(APIView):
    def get(self, request):
        state = '/'
        oauth = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                        client_secret=settings.QQ_CLIENT_SECRET,
                        redirect_uri=settings.QQ_REDIRECT_URI,
                        state=state)
        auth_url = oauth.get_qq_url()
        # 拼接url
        return Response({'auth_url': auth_url})


"""
1、当用户同意登录的时候，会返回code
2、用code 换取token
3、用token 换取openid

"""

"""
当用户同意登录的时候，会生成code，前端需要将获取的code提交给后端
GET   /oauth/qq/user/?code=xxxx
"""
from rest_framework import status
class OauthQQUserAPIView(APIView):
    def get(self,request):
        code = request.query_params.get('code')
        # 获取前端传来的code
        if code is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        oauth = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                        client_secret=settings.QQ_CLIENT_SECRET,
                        redirect_uri=settings.QQ_REDIRECT_URI,
        )
        access_token = oauth.get_access_token(code)
        # 用code换取token
        openid = oauth.get_open_id(access_token)
        # 用token换取openid
        # 获取到openid后，要根据这个openid去数据库中查询，如果数据库中有此信息，说明用户绑定过，应该让用户登录，如果没有此信息，说明没有绑定过，应该让用户绑定。
        try:
            qquser = OAuthQQUser.objects.get(openid=openid)
        except OAuthQQUser.DoesNotExist:
            # 说明用户没有绑定过
            # openid比较敏感，我们要对openid进行处理，对绑定设置一个时效。
            token = generic_open_id(openid)
            # 对openid的处理，设置时效，然后传给前端
            return Response({'access_token': token})
        else:
            # 说明用户绑定过
            # 绑定过就应该登录，获取用户的token，然后传给前端
            from rest_framework_jwt.settings import api_settings
            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
            payload = jwt_payload_handler(qquser.user)
            token = jwt_encode_handler(payload)
            # 以上几行为获取token的过程。
            return Response({
                'token':token,
                'username':qquser.user.username,
                'user_id':qquser.user.id
            })

            pass

    """
    当用户绑定的时候，需要让前端传递手机号，密码，短信验证码和加密的openid
    # 1 接收数据
    # 2 验证数据
            1 openid
            2 短信验证码
            3 根据手机号进行判断，手机号是否注册过
    # 3 数据入库
    # 4 返回响应
    POST
    """
    def post(self, request):
        # 1 接收数据
        data = request.data
        # 2 验证数据
        serializer = OauthQQUserSerializer(data = data)
        serializer.is_valid(raise_exception=True)
        # 3 数据入库
        qquser= serializer.save()
        # 4 返回响应(以下5行为获取token)
        from rest_framework_jwt.settings import api_settings
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(qquser.user)
        token = jwt_encode_handler(payload)

        return Response({
            'token': token,
            'username': qquser.user.username,
            'userid': qquser.user.id
        })










