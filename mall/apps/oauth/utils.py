from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature,SignatureExpired
from mall import settings


def generic_open_id(openid):
    s = Serializer(secret_key=settings.SECRET_KEY,expires_in=3600)
    data = {
        'openid':openid
    }
    # 对数据加密
    token = s.dumps(data)
    return token.decode()


def check_access_token(token):
     s = Serializer(secret_key=settings.SECRET_KEY,expires_in=3600)

     try:
         data = s.loads(token)
         # 对数据解密
     except BadSignature:
         return None

     return data.get('openid')


#########################以下是范例 itsdangerous 的用法#################################
# from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
# from mall import settings
# #1. 创建序列化器实例
# #secret_key         秘钥
# #  expires_in=None  过期时间 单位是: 秒数
# s = Serializer(secret_key=settings.SECRET_KEY,expires_in=60*60)
#
# #2. 组织数据
# data = {
#     'openid':'1234567890'
# }
#
# #3.对数据进行处理
# token = s.dumps(data)
#
# #eyJpYXQiOjE1NDcxMTIyNTIsImFsZyI6IkhTMjU2IiwiZXhwIjoxNTQ3MTE1ODUyfQ.
# # eyJvcGVuaWQiOiIxMjM0NTY3ODkwIn0.
# # _v-vRCPopYDahqt81MdftYfmswxOBU0gGwL7xXGDHaQ
#
# # 4. 对数据进行解密
# s.loads(token)
#
#
# s = Serializer(secret_key=settings.SECRET_KEY,expires_in=1)
#
# #2. 组织数据
# data = {
#     'openid':'1234567890'
# }
#
# #3.对数据进行处理
# token = s.dumps(data)
