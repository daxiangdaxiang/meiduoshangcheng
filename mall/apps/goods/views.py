from django.shortcuts import render
from rest_framework.generics import GenericAPIView, ListAPIView
from rest_framework.mixins import ListModelMixin
from goods.models import SKU
from utils.pagination import StandardResultsSetPagination
from .serializer import SKUhotListSerializer

"""
列表数据分为热销数据和列表数据

热销数据

前端将分类id传给后端，
1 接收数据 category_id
2 根据id获取数据
3 将对象列表转换成字典
4 返回响应

GET /goods/categories/category_id/hotskus/
"""
class SKUhotListAPIView(ListAPIView):
    pagination_class = None
    serializer_class = SKUhotListSerializer
    def get_queryset(self):
        # category_id通过路由 传递给SKUhotListAPIView ，在self里面。
        # 不能在属性和类中打断点，所以，改成def 方法，通过打断点，查看具体位置在 kwargs里面， 所以，通过self.kwargs['category_id']获取
        category_id = self.kwargs['category_id']
        return SKU.objects.filter(category=category_id).order_by('-sales')[:2]


from rest_framework.filters import OrderingFilter
from rest_framework.pagination import LimitOffsetPagination,PageNumberPagination
class SKUListAPIView(ListAPIView):

    serializer_class = SKUhotListSerializer
    # 排序
    filter_backends = [OrderingFilter]
    ordering_fields = ['create_time','price','sales']

    # 分页
    # permission_classes = LimitOffsetPagination           # 方法1
    # permission_classes = StandardResultsSetPagination    # 方法2
    # 只给某个（这个）视图设置分页
    # 现在分页设置在settings里面设置的，默认所有的视图都分页
    def get_queryset(self):
        category_id = self.kwargs['category_id']
        return SKU.objects.filter(category=category_id)


from .serializer import SKUIndexSerializer
from drf_haystack.viewsets import HaystackViewSet

class SKUSearchViewSet(HaystackViewSet):
    """
    SKU搜索
    """
    index_models = [SKU]

    serializer_class = SKUIndexSerializer