from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from django_redis import get_redis_connection
from rest_framework.response import Response
from goods.models import SKU
from orders.serizlizers import PlaceOrdersSerializer, PlaceSerializer

"""
用户点击结算的时候，必须是登录状态

1 连接redis
2 获取hash
     set
3 选中商品的数据，对数据进行类型转换
4 从reids中获取选中商品id
5 根据id进行数据查询
6 将列表数据转换为字典数据
7 返回响应
"""

class PlaceOrdersAPIView(APIView):
    """提交购物车商品"""
    permission_classes = [IsAuthenticated]

    def get(self,request):
        user = request.user
        # 1 连接redis获取数据
        redis_conn = get_redis_connection('cart')
        redis_id_count = redis_conn.hgetall('cart_%s'%user.id)
        selected_ids = redis_conn.smembers('cart_selected_%s'%user.id)
        # 2 选中商品的数据，对数据进行类型转换
        selected_cart = {}
        for sku_id in selected_ids:
            selected_cart[int(sku_id)] = int(redis_id_count[sku_id])
        # 3 从selected_cart中获取选中商品的id
        ids = selected_cart.keys()
        # 4 根据id进行数据的查询
        skus = SKU.objects.filter(pk__in=ids)
        for sku in skus:
            sku.count = selected_cart[sku.id]
        # 5 将列表数据转化成字典数据
        # serializer = PlaceOrdersSerializer(skus, many=True)
        # return Response(serializer.data)
        # 需要添加运费信息
        # data = {
        #     'freight':10,
        #     'skus':serializer.data
        # }
        # return Response(data)

        serializer = PlaceSerializer({
            'freight':10,
            'skus':skus
        })

        return Response(serializer.data)


"""
提交订单
1 这个接口必须是登录用户访问
2 商品信息从redis获取

前端传的数据： 用户信息  地址  支付方式

后端：
1 接收数据
2 对数据进行校验
3 数据入库
4 返回响应

POST  /orders/
"""

from rest_framework.generics import CreateAPIView
from .serizlizers import OrderCommitSerializer

class OrderAPIView(CreateAPIView):
    serializer_class = OrderCommitSerializer