from rest_framework import serializers

from goods.models import SKU


class PlaceOrdersSerializer(serializers.ModelSerializer):

    count = serializers.IntegerField(required=True,label='商品数量')


    class Meta:
        model = SKU
        fields = ['id','price','count','default_image_url','name']

    def validate(self, attrs):
        count = attrs.get('count')
        if count > SKU.stock:
            raise serializers.ValidationError('库存不足')
        return attrs

class PlaceSerializer(serializers.Serializer):

    freight = serializers.DecimalField(label='运费',max_digits=10, decimal_places=2)
    skus = PlaceOrdersSerializer(many=True)

from orders.models import OrderInfo,OrderGoods

class OrderCommitSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderInfo
        fields = ('order_id', 'address', 'pay_method')
        read_only_fields = ('order_id',)
        extra_kwargs = {
            'address': {
                'write_only': True,
                'required': True,
            },
            'pay_method': {
                'write_only': True,
                'required': True
            }
        }


    def create(self, validated_data):
        pass

