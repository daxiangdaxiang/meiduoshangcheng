from rest_framework.urls import url
from . import views
urlpatterns = [
    url(r'^places/$', views.PlaceOrdersAPIView.as_view()),
    url(r'^$', views.OrderAPIView.as_view(),name='order'),
]

